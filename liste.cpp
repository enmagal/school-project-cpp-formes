#include <iostream>
#include "liste.hpp"

int Liste::compteur = 0;

Liste::Liste(): nb_c(0), nb_r(0)
{
    Liste::compteur ++;
}

Liste::~Liste()
{
}

int Liste::getCompteur()
{
    return Liste::compteur;
}

void Liste::addCercle(Cercle & nouv)
{
    cercles[nb_c] = &nouv;
    nouv.setOrdre(nb_c + nb_r);
    nb_c ++;
}

void Liste::addRectangle(Rectangle & nouv)
{
    rectangles[nb_r] = new Rectangle(nouv);
    nouv.setOrdre(nb_c+nb_r);
    nb_r ++;
}

std::string Liste::toString()
{
    int i = 0;
    std::string chaine = "LISTE : ";
    for (i=0; i<nb_r; i++)
    {
        chaine += rectangles[i]->toString();
        chaine += "; ";
    }

    for (i=0; i<nb_c; i++)
    {
        chaine += cercles[i]->toString();
        chaine += "; ";
    }
    return chaine;
}