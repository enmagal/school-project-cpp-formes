#ifndef POINT
#define POINT

class Point
{
private:
    int x;
    int y;

public:
    Point(int, int);
    ~Point();
    int getX();
    int getY();
    void setX(int);
    void setY(int);
};

#endif