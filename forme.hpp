#ifndef FORME
#define FORME

#include "point.hpp"

class Forme
{
private:
    Point point;
    int w;
    int h;
    static int nbFormes;

public:
    Forme(Point, int, int);
    ~Forme();
    int getW();
    int getH();
    int getX();
    int getY();
    void setH(int);
    void setW(int);
    void setPoint(int, int);
    static int getNbFormes();
};


#endif