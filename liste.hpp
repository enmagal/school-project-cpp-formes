#ifndef LISTE
#define LISTE

#include "rectangle.hpp"
#include "cercle.hpp"

const int TAILLE = 10;

class Liste
{
private:
    Cercle * cercles[TAILLE];
    int nb_c;
    Rectangle * rectangles[TAILLE];
    int nb_r;
    static int compteur;

public:
    Liste();
    ~Liste();
    static int getCompteur();
    std::string toString();
    void addCercle(Cercle &);
    void addRectangle(Rectangle &);
};


#endif