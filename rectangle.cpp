#include <iostream>
#include <sstream>
#include "rectangle.hpp"

Rectangle::Rectangle(int vx, int vy, int vw, int vh): x(vx), y(vy), w(vw), h(vh), ordre(-1)
{
}

std::string Rectangle::toString()
{
    std::ostringstream oss;
    std::string chaine = "RECTANGLE " + std::to_string(x) + " " + std::to_string(y) + " " + std::to_string(w) + " " + std::to_string(h);
    return chaine;
}

int Rectangle::getOrdre() const
{
    return ordre;
}

void Rectangle::setOrdre(int val)
{
    ordre = val;
}