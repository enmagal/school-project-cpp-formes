#include <iostream>
#include "point.hpp"

Point::Point(int vx, int vy): x(vx), y(vy)
{
}

Point::~Point()
{
}

int Point::getX()
{
    return x;
}

int Point::getY()
{
    return y;
}

void Point::setX(int vx)
{
    x = vx;
}

void Point::setY(int vy)
{
    y = vy;
}