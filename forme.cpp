#include <iostream>
#include "forme.hpp"

int Forme::nbFormes = 0;

Forme::Forme(Point vpoint, int vw, int vh): point(vpoint), w(vw), h(vh)
{
    nbFormes++;
}

Forme::~Forme()
{
}

int Forme::getW(){
    return w;
}

int Forme::getH(){
    return h;
}

int Forme::getX(){
    return point.getX();
}

int Forme::getY(){
    return point.getY();
}

void Forme::setH(int vh)
{
    h = vh;
}

void Forme::setW(int vw)
{
    w = vw;
}

void Forme::setPoint(int vx, int vy)
{
    point.setX(vx);
    point.setY(vy);
}

int Forme::getNbFormes()
{
    return nbFormes;
}