#include <iostream>
#include <string>
#include "rectangle.hpp"
#include "cercle.hpp"
#include "liste.hpp"
#include "point.hpp"
#include "forme.hpp"

int main(int ,char**)
{
    Rectangle rec(3, 3, 5, 5);
    std::cout << "Hello world" << std::endl;
    std::string chaine = rec.toString();
    std::cout << chaine << std::endl;

    Cercle cer(3, 3, 5);
    std::string chaine2 = cer.toString();
    std::cout << chaine2 << std::endl;

    Liste liste;
    liste.addCercle(cer);
    liste.addRectangle(rec);

    std::cout << cer.getOrdre() << std::endl;
    std::cout << rec.getOrdre() << std::endl;

    Point p(4, 6);

    std::string chaine3 = liste.toString();
    std::cout << chaine3 << std::endl;

    Forme forme(p, 5, 7);
    /*std::cout << forme.getX() << std::endl;
    std::cout << forme.getY() << std::endl;
    std::cout << forme.getH() << std::endl;
    forme.setPoint(5, 9);
    std::cout << forme.getX() << std::endl;
    std::cout << forme.getY() << std::endl;
    std::cout << forme.getH() << std::endl;*/
    Forme forme2(p, 8, 9);
    Forme forme3(p, 6, 9);

    std::cout << "le nombre de forme est : " << Forme::getNbFormes() << std::endl;
    return 0;
}