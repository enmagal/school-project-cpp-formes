#ifndef CERCLE
#define CERCLE

class Cercle
{
private:
    int x;
    int y;
    int w;
    int h;
    int ordre;

public:
    int getOrdre() const;
    void setOrdre(int);
    Cercle(int, int, int, int);
    Cercle(int, int, int);
    ~Cercle();
    std::string toString();
};



#endif