#include <iostream>
#include <sstream>
#include "cercle.hpp"

Cercle::Cercle(int vx, int vy, int vw, int vh): x(vx), y(vy), w(vw), h(vh), ordre(-1)
{
}

Cercle::Cercle(int vx, int vy, int vr): x(vx), y(vy), w(2*vr), h(2*vr)
{
}

Cercle::~Cercle()
{
}

std::string Cercle::toString()
{
    std::ostringstream oss;
    std::string chaine = "CERCLE " + std::to_string(x) + " " + std::to_string(y) + " " + std::to_string(w) + " " + std::to_string(h);
    return chaine;
}

int Cercle::getOrdre() const
{
    return ordre;
}

void Cercle::setOrdre(int val)
{
    ordre = val;
}