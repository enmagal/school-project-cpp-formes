#ifndef RECTANGLE
#define RECTANGLE
#include <string>

class Rectangle{
    private :
        int x;
        int y;
        int w;
        int h;
        int ordre;
    
    public :
        int getOrdre() const;
        void setOrdre(int);
        Rectangle(int, int, int, int);
        std::string toString();
};

#endif